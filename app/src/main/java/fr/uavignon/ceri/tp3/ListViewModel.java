package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Musee;
import fr.uavignon.ceri.tp3.data.MuseeRepository;


   public class ListViewModel extends AndroidViewModel {

       private MuseeRepository repository;
       private LiveData<List<Musee>> allMusees;
       private LiveData<List<Musee>> allMuseesOrderedByYear;


       public ListViewModel(@NonNull Application application) {
           super(application);
           repository = MuseeRepository.get(application);
           allMusees = repository.getallMusees();
           allMuseesOrderedByYear = repository.getallMuseesOrderedByYear();
       }

       LiveData<List<Musee>> getallMusees() {
           return allMusees;
       }

       LiveData<List<Musee>> getallMuseesOrderedByYear() {
           return allMuseesOrderedByYear;
       }




   }

