package fr.uavignon.ceri.tp3.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.MuseeDao;
import fr.uavignon.ceri.tp3.data.database.MuseeRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;

import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.MuseeRoomDatabase.databaseWriteExecutor;


public class MuseeRepository {

    private static final String TAG = MuseeRepository.class.getSimpleName();

    private LiveData<List<Musee>> allMusees;
    private LiveData<List<Musee>> allMuseesOrderedByYear;
    private MutableLiveData<Musee> selectedMusee;
    private final OWMInterface api;
    private MuseeDao museeDao;

    public static volatile  MuseeRepository INSTANCE;

    public synchronized static MuseeRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseeRepository(application);
        }

        return INSTANCE;
    }



    public MuseeRepository(Application application){
        MuseeRoomDatabase db = MuseeRoomDatabase.getDatabase(application);
        museeDao = db.museeDao();
        allMusees = museeDao.getAllMusees();
        allMuseesOrderedByYear = museeDao.getAllMuseesOrderByYear();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(OWMInterface.class);
        if(allMusees.getValue() == null || allMusees.getValue().size() == 0){
            loadallMusees();
            allMusees = museeDao.getAllMusees();
        }
        selectedMusee = new MutableLiveData<>();
    }


    public LiveData<List<Musee>> getallMusees() {
        return allMusees;
    }

    public LiveData<List<Musee>> getallMuseesOrderedByYear() {
        return allMuseesOrderedByYear;
    }

    public MutableLiveData<Musee> getselectedMusee() {
        return selectedMusee;
    }

    public long insertMusee(Musee musee) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return museeDao.insert(musee);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedMusee.setValue(musee);
        return res;
    }

    public void getMusee(String id){
        Future<Musee> fMusee = databaseWriteExecutor.submit(() -> {
            return museeDao.getMuseeById(id);
        });
        try {
            selectedMusee.setValue(fMusee.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadallMusees(){

        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call, Response<Map<String, ItemResponse>> response) {
                        List<Musee> musees = new ArrayList<Musee>();
                        if(response.body() != null){
                            ItemResult.transferInfo(response.body(), musees);
                            for(Musee m : musees){
                                insertMusee(m);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {

                    }
                }
        );
    }

}
