package fr.uavignon.ceri.tp3;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;
import fr.uavignon.ceri.tp3.data.Musee;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<Musee> museeList;
    private List<Musee> defaultMuseeList;
    private SearchView searchView;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if(viewType == 0){
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.left_layout, parent, false);
        }else{
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.right_layout, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {
        ViewHolder holder = (ViewHolder) viewholder;
        holder.deviceName.setText(museeList.get(position).getName());
        holder.deviceBrand.setText(museeList.get(position).getBrand());
        if(museeList.get(position).getYear() != null){
            holder.deviceYear.setText(museeList.get(position).getYear().toString());
        }else{
            holder.deviceYear.setText("");
        }

        String[] list = museeList.get(position).getCategories().split("#");
        StringBuilder string = new StringBuilder();
        for (String s : list){
            string.append(s).append(" - ");
        }
        string.delete(string.length()-3, string.length());
        holder.deviceCategories.setText(string.toString());
        Glide.with(holder.deviceImage.getContext())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + museeList.get(position).getId() + "/thumbnail")
                .into(holder.deviceImage);

    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    @Override
    public int getItemCount() {
        if(museeList != null){
            return getSearchedMusees().size();
        }
        return 0;
    }

    public List<Musee> getSearchedMusees(){
        if(defaultMuseeList == null) return new ArrayList<Musee>();
        CharSequence searchValue = searchView.getQuery();
        List<Musee> cMusees = new ArrayList<>();
        if(searchValue.length() != 0){
            for(Musee musee: defaultMuseeList){

                if(musee.getName().toLowerCase().contains(searchValue.toString().toLowerCase())){
                    if(musee.getYear() != null){

                        Log.e(null, musee.getName() + " " + musee.getYear().toString());
                    }else{
                        Log.e(null, musee.getName());
                    }
                    cMusees.add(musee);
                }
            }
        }else{
            cMusees = defaultMuseeList;
        }
        return  cMusees;
    }

    public void setDefaultMuseeList(List<Musee> defaultMuseeList){
        this.defaultMuseeList = defaultMuseeList;
    }

    public void setSearchView(SearchView searchView){
        this.searchView = searchView;

        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setMuseeList(museeList);
                return false;
            }
        });
    }

    public void setMuseeList(List<Musee> musees) {
        museeList = getSearchedMusees();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView deviceName;
        TextView deviceBrand;
        TextView deviceCategories;
        TextView deviceYear;
        ImageView deviceImage;


        ViewHolder(View itemView) {
            super(itemView);
            deviceName = itemView.findViewById(R.id.device_name);
            deviceBrand = itemView.findViewById(R.id.device_brand);
            deviceCategories = itemView.findViewById(R.id.device_categories);
            deviceImage = itemView.findViewById(R.id.device_image);
            deviceYear = itemView.findViewById(R.id.device_year);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    String id2 = RecyclerAdapter.this.museeList.get((int)getAdapterPosition()).getId();

                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment();
                    action.setCityNum(id2);
                    Navigation.findNavController(v).navigate(action);

                }
            });

        }

    }

}
