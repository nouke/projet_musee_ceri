package fr.uavignon.ceri.tp3.data;

import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;

import java.util.Map;
import java.util.List;
public class ItemResult {
    public static void transferInfo(Map<String, ItemResponse> map, List<Musee> musees){
        for (Map.Entry<String, ItemResponse> entry: map.entrySet()){
            Musee musee = new Musee(entry.getKey(), entry.getValue().name);
            musee.setDescription(entry.getValue().description);
            musee.setTechnicalDetails(entry.getValue().technicalDetails);
            musee.setCategories(entry.getValue().categories);
            musee.setPictures(entry.getValue().pictures);
            musee.setWorking(entry.getValue().working);
            musee.setYear(entry.getValue().year);
            musee.setBrand(entry.getValue().brand);
            musee.setTimeFrame(entry.getValue().timeFrame);
            musees.add(musee);
        }

    }
}

