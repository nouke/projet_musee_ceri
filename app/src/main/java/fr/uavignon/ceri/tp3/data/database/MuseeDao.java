package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Musee;


@Dao
public interface MuseeDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Musee musee);

    @Query("SELECT * FROM musee_table ORDER BY name")
    LiveData<List<Musee>> getAllMusees();
    ;
    @Query("SELECT * FROM musee_table ORDER BY year ASC")
    LiveData<List<Musee>> getAllMuseesOrderByYear();

    @Query("SELECT * FROM musee_table WHERE id = :id")
    Musee getMuseeById(String id);


}
