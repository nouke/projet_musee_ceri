package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;


//api.openweathermap.org/data/2.5/weather?q=Avignon,FR&appid=77af22b80c4ab3279df6e3678897d463

public interface OWMInterface {

    //String PATHS = "cerimuseum";

    //Interrogation du service web

   @Headers({
            "Accept: json",
    })
   @GET("collection")
    Call<Map<String, ItemResponse>> getCollection();

}
