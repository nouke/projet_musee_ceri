package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Map;
import java.util.List;


@Entity(tableName = "musee_table", indices = {@Index(value = {"name"}, unique = true)})
public class Musee {
    public static final String TAG = Musee.class.getSimpleName();

    public static final long ADD_ID = -1;

    @PrimaryKey()
    @NonNull
    @ColumnInfo(name="id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="year")
    private Integer year;

    @ColumnInfo(name="working")
    private Boolean working;

    @ColumnInfo(name="categories")
    private String categories;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name="pictures")
    private String pictures;

    @ColumnInfo(name="timeFrame")
    private String timeFrame;

    public Musee(@NonNull String id, @NonNull String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        StringBuilder string = new StringBuilder();
        for (String s : categories){
            string.append(s).append("#");
        }
        string.delete(string.length()-1, string.length());

        this.categories = string.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        if(technicalDetails == null) return;
        StringBuilder string = new StringBuilder();
        for (String s : technicalDetails){
            string.append(s).append("#");
        }
        string.delete(string.length()-1, string.length());

        this.technicalDetails = string.toString();
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        if(pictures == null) return;
        StringBuilder mapAsString = new StringBuilder("{");
        for(String key: pictures.keySet()){
            mapAsString.append(key + "=" + pictures.get(key) + "#");
        }
        mapAsString.delete(mapAsString.length()-2, mapAsString.length()).append("}");
        this.pictures = mapAsString.toString();
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(List<Integer> timeFrame) {
        String string = timeFrame.toString();
        this.timeFrame = string.substring(1, string.length()-1);
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

     public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }
}

