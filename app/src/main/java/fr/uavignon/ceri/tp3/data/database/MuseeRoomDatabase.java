package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import fr.uavignon.ceri.tp3.data.Musee;


@Database(entities = {Musee.class}, version = 1, exportSchema = false)
public abstract class MuseeRoomDatabase extends RoomDatabase {

    private static final String TAG = fr.uavignon.ceri.tp3.data.database.MuseeRoomDatabase.class.getSimpleName();

    public abstract MuseeDao museeDao();

    private static fr.uavignon.ceri.tp3.data.database.MuseeRoomDatabase INSTANCE;
    private static  final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static MuseeRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null) {
            synchronized (fr.uavignon.ceri.tp3.data.database.MuseeRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    fr.uavignon.ceri.tp3.data.database.MuseeRoomDatabase.class,"musee_database")
                                    .build();
                }
            }
        }
        return INSTANCE;
    }

}


