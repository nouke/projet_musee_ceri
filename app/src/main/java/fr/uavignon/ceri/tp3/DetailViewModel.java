package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Musee;
import fr.uavignon.ceri.tp3.data.MuseeRepository;


public class DetailViewModel  extends AndroidViewModel {

    private MuseeRepository repository;
    private MutableLiveData<Musee> musee;

    public DetailViewModel (Application application){
        super(application);
        repository = MuseeRepository.get(application);
        musee = new MutableLiveData<>();
    }

    public void setMusee(String id){
        repository.getMusee(id);
        musee = repository.getselectedMusee();
    }



    public LiveData<Musee> getMusee(){
        return musee;
    }



}
