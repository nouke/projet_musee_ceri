package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import fr.uavignon.ceri.tp3.data.PictureAdapter;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView deviceDetailName, deviceDetailDescription, deviceDetailYear, deviceDetailBrand,
            deviceTechnicalDetails, deviceDetailCategories, deviceDetailTimeFrame;
    private ImageView deviceDetailImage;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PictureAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);


        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String deviceId = args.getCityNum();
        viewModel.setMusee(deviceId);
        listenerSetup();
        observerSetup();

    }

    private void listenerSetup(){
        deviceDetailBrand = getView().findViewById(R.id.device_brand);
        deviceDetailName = getView().findViewById(R.id.device_name);
        deviceDetailDescription = getView().findViewById(R.id.device_description);
        deviceDetailYear = getView().findViewById(R.id.device_year);
        deviceDetailImage = getView().findViewById(R.id.device_image);
        deviceTechnicalDetails = getView().findViewById(R.id.device_technicalDetails);
        deviceDetailCategories = getView().findViewById(R.id.device_categories);
        deviceDetailTimeFrame = getView().findViewById(R.id.device_timeFrame);

        adapter = new PictureAdapter();
        recyclerView = getView().findViewById(R.id.picture_recycler_view);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);

        getView().findViewById(R.id.button_previous).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup(){
        viewModel.getMusee().observe(getViewLifecycleOwner(),
                musee -> {
                    if(musee != null){
                        adapter.setCurrentMusee(musee);
                        adapter.setPictures(musee.getPictures());
                        deviceDetailBrand.setText(musee.getBrand());
                        deviceDetailDescription.setText(musee.getDescription());
                        deviceDetailName.setText(musee.getName());
                        if(musee.getTechnicalDetails() != null){
                            String[] list = musee.getTechnicalDetails().split("#");
                            StringBuilder string = new StringBuilder();
                            for (String s : list){
                                string.append(s).append(" - ");
                            }
                            string.delete(string.length()-3, string.length());
                            String toShow = getString(R.string.technicalDetailsString) + string.toString();
                            deviceTechnicalDetails.setText(toShow);
                        }
                        if(musee.getTimeFrame() != null){
                            String toShow = getString(R.string.timeFrameString) + musee.getTimeFrame();
                            deviceDetailTimeFrame.setText(toShow);
                        }

                        String[] list = musee.getCategories().split("#");
                        StringBuilder string = new StringBuilder();
                        for (String s : list){
                            string.append(s).append(" - ");
                        }
                        string.delete(string.length()-3, string.length());
                        String toShow = getString(R.string.detailCategoriesString) + string.toString();
                        deviceDetailCategories.setText(toShow);

                        if(musee.getYear() != null) deviceDetailYear.setText(musee.getYear().toString());
                        Glide.with(deviceDetailImage.getContext())
                                .load("https://demo-lia.univ-avignon.fr/cerimuseum/" + musee.getId() + "/thumbnail")
                                .into(deviceDetailImage);
                    }
                }
        );
    }
}
